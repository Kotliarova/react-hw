import styled from "styled-components";

export const StyledButton = styled.button`
  background-color: ${(props) => props.backgroundColor};
  font-size: 25px;
  color: white;
  cursor: pointer;
  border: none;
  border-radius: 5px;
  padding: 5px;
  width: 25%;
`;

export const StyledButtonClose = styled.button`
  background-color: transparent;
  font-size: 25px;
  cursor: pointer;
  border: none;
  color: #fff;
  position: absolute;
  right: 20px;
  top: 20px;
`;

 export const StyledButtonAction = styled.button`
  background-color: #b3382c;
  font-size: 25px;
  color: white;
  cursor: pointer;
  border: none;
  border-radius: 5px;
  padding: 10px;
  width: 25%;
  margin 10px 10px 0;
`;

export const StyledButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
`;