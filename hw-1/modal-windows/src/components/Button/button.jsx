import { Component } from 'react';
import { StyledButton, StyledButtonClose, StyledButtonAction } from './button.styles';


export class Button extends Component {
    constructor(props) {
        super(props)
    }

    render(){
        const { backgroundColor, text, onClick, id } = this.props;
        return (
        <StyledButton
            id={id}
            backgroundColor={backgroundColor}
            onClick={onClick}>
            {text}
        </StyledButton>)
    }
}

export class ButtonClose extends Button {
    constructor(props) {
        super(props)
    }
    render(){
        const { onClick } = this.props;
        return (
        <StyledButtonClose
            onClick={onClick}>
            {'X'}
        </StyledButtonClose>)
    }
}

export class ButtonAction extends Button {
    constructor(props) {
        super(props)
    }
    render(){
        const { onClick, action } = this.props;
        return (
        <StyledButtonAction
            onClick={onClick}>
            {action}
        </StyledButtonAction>)
    }
}
