import { Component } from 'react';
import { StyleModal, StyleModalHeader, StyleModalText } from './modal.styles';
import { ButtonClose, ButtonAction } from '../Button/button';
import { StyledButtonWrapper } from '../Button/button.styles';
import { collectModals } from './modalData';


export class Modal extends Component {
    constructor(props) {
        super(props)
    }

    deleteFile = () => {
        console.log ('deleted')
    }

    saveFile = () => {
        console.log ('saved')
    }


    render() {
    const { closeButton, closeModal } = this.props;
    const { modalId } = this.props;
    const modal = collectModals.find(item => item.id === modalId);

    return (
      <StyleModal>
        <StyleModalHeader>{modal.header}
        {closeButton ? <ButtonClose 
                onClick={closeModal}
            /> : null}
        </StyleModalHeader>
        <StyleModalText>{modal.text}</StyleModalText>
        <StyledButtonWrapper>
            {modal.actions[0] === 'ok' ? <ButtonAction action={modal.actions[0]} onClick={(e) => {this.deleteFile(); closeModal(e)}}/> : null}
            {modal.actions[0] === 'save' ? <ButtonAction action={modal.actions[0]} onClick={(e) => {this.saveFile(); closeModal(e)}}/> : null}
            {modal.actions.length === 2 ? <ButtonAction action={modal.actions[1]} onClick={closeModal}/> : null}
        </StyledButtonWrapper>
      </StyleModal>
    );
    }
}