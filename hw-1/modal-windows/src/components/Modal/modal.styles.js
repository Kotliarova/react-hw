import styled from "styled-components";

export const StyleModal = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #e74c3c;
    width: 50%;
    padding-bottom: 20px;
    z-index: 1000;
`;

export const StyleModalHeader = styled.div`
    color: #fff;
    background-color: #d44637;
    padding: 20px;
    font-size: 25px;
    font-weight: 600;
`;

export const StyleModalText = styled.div`
    color: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    line-height: 1.5;
    padding: 20px;
    font-size: 20px;
    text-align: center;
`;

export const ModalBackground = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 999;
`;