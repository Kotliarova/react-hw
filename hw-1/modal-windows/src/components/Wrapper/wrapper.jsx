import { Component } from 'react';
import { StyledWrapper } from './wrapper.styles';
import { Button } from '../Button/button';
import { Modal } from '../Modal/modal';
import { collectModals } from '../Modal/modalData';
import { ModalBackground } from '../Modal/modal.styles';


class Wrapper extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openModalId: ''
        }
    }

    openModal = (event) => {
        const btnId = event.target.id;
        const currentModal = collectModals.find(modal => modal.id === btnId);
        this.setState({ openModalId: currentModal.id });
    }

    closeModal = (e) => {
        if (e.target === e.currentTarget) { 
            this.setState({ openModalId: '' });
        }
    }

    render(){
        
        return (
        <StyledWrapper>
            <Button 
                id={1}
                backgroundColor={'#ff0000'}
                onClick={this.openModal}
                text={'Open first modal'}
            />
             <Button 
                id={2}
                backgroundColor={'#800000'}
                onClick={this.openModal}
                text={'Open second modal'}
            />
            {this.state.openModalId ? <ModalBackground onClick={this.closeModal}> 
                <Modal 
                closeButton={true}
                closeModal={this.closeModal}
                modalId={this.state.openModalId}
            /></ModalBackground> : null}
        </StyledWrapper>)
    }
}

export default Wrapper