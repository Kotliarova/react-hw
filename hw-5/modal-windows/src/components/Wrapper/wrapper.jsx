import { StyledWrapper } from './wrapper.styles';
import { ListItem } from '../ListItem/list-item';
import { Header } from '../Header/header';




function Wrapper({items}) {   
    return (
        <StyledWrapper>
            <Header></Header>
            <ListItem items={items} inCart={false}/>
        </StyledWrapper>
    )
}

export default Wrapper