import { StyleModal, StyleModalHeader, StyleModalText } from './modal.styles';
import { Button } from '../Button/button';
import { StyledButtonWrapper } from '../Button/button.styles';
import { collectModals } from './modalData';
import PropTypes from 'prop-types'
import { useSelector, useDispatch} from 'react-redux';
import { addToCart, removeFromCart } from '../../redux/actions/items';

export function Modal({ closeButton, closeModal}) {
    const modalId = useSelector(state => state.modal.modalId)
    const modal = collectModals.find(el => el.id === modalId);
    const currentItem = useSelector(state => state.items.currentItem)
    const dispatch = useDispatch()


    return (
        <StyleModal>
           <StyleModalHeader>{modal.header}
                {closeButton ? <Button 
                onClick={closeModal} 
                className={'btn-close'}
                content={'X'}
            /> : null}
        </StyleModalHeader>
        <StyleModalText>{modal.text}</StyleModalText>
        <StyledButtonWrapper>
            {modal.actions[0] === 'Yes' && modal.id === '3' ? <Button className={'btn-action'} content={modal.actions[0]} onClick={(e) => {dispatch(addToCart(currentItem)); closeModal(e)}}/> : null}
            {modal.actions[0] === 'Yes' && modal.id === '1' ? <Button className={'btn-action'} content={modal.actions[0]} onClick={(e) => {dispatch(removeFromCart(currentItem)); closeModal(e)}}/> : null}
            {modal.actions.length === 2 && modal.id === '3'? <Button className={'btn-action'} content={modal.actions[1]} onClick={(e) => {closeModal(e)}}/> : null}
            {modal.actions.length === 2 && modal.id === '1'? <Button className={'btn-action'} content={modal.actions[1]} onClick={(e) => {closeModal(e)}}/> : null}
        </StyledButtonWrapper>
      </StyleModal>
    )
}

Modal.propTypes = {
    closeButton: PropTypes.bool, 
    closeModal: PropTypes.func,
    cart: PropTypes.func,
    modalId: PropTypes.string
}

Modal.defaultProps = {
    closeButton: true
}