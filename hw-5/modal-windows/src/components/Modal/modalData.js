export const collectModals = [
    {
    id: "1",
    header: "Do you want to delete this product from cart?",
    text: "Once you delete this product, after you can add this one time more. Are you sure you want to delete this?",
    actions: ['Yes', 'No']
    },
    {
    id: "2",
    header: "Do you want to save this file?",
    text: "description for modal 2",
    actions: ['save', 'delete']
    },
    {
    id: "3",
    header: "Do you want to add this product to the cart?",
    text: "Click Yes to add to cart",
    actions: ['Yes', 'No']
    }
    
]
