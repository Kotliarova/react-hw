import './button.scss'
import PropTypes from 'prop-types'


export function Button({ className, onClick, content }) {
    return (
        <button className={className}
            onClick={onClick}>
            {content}            
        </button>)
}


Button.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    countFavorite: PropTypes.number,
    countCart: PropTypes.number
}

Button.defaultProps = {
    content: null,
    countFavorite: 0,
    countCart: 0
}
