import { Item } from "../Item/item";
import './list-item.scss'
import PropTypes from 'prop-types'
import { useSelector } from "react-redux";


export function ListItem({items}) {
    const fav = useSelector(state => state.items.favoriteArr)
    const cart = useSelector(state => state.items.cartArr)
    const itemsList = items && items.map(item => (
        <Item 
            key={item.article}
            item={item}
            isFav={fav.findIndex((obj) => obj.article === item.article) !== -1}
            inCart={cart.findIndex((obj) => obj.article === item.article) !== -1}/>
    ));
    return <div className="list-item">{itemsList}</div>
}

ListItem.propTypes = {
    favorite: PropTypes.func,
    cart: PropTypes.func,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            article: PropTypes.string
        })
    )
}

ListItem.defaultProps = {
    items: [
      { article: "000", urlPhoto: "#", name: "something", price: 0 }
    ]
  }