
// При коректній інформації в полях після натискання на Checkout викликаємо action, який у свою чергу очищає з localStorage дані про кошик. При цьому в консоль має бути виведено інформацію про придбані товари, а також інформацію, яку користувач заповнив у формі.


import './form.scss'
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import {PatternFormat} from 'react-number-format';
import { useDispatch, useSelector } from 'react-redux';
import {clearCart} from '../../redux/actions/items'


const Error = ({ children }) => (
    <div className="message-error">{children}</div>
  );
  

export function FormCart() {
    const dispatch = useDispatch()
    const currentCart = useSelector(state => state.items.cartArr)
    const validationSchema = Yup.object().shape({
        firstName: Yup.string()
            .max(10, 'Must be 10 letters or less')
            .matches(/^[A-Z][a-z]+$/, 'First name should start with a capital letter and contain only lowercase letters on lattin')
            .required('First name is required'),
        lastName: Yup.string()
            .max(20, 'Must be 20 letters or less')
            .matches(/^[A-Z][a-z]+$/, 'First name should start with a capital letter and contain only lowercase letters on lattin')
            .required('Last name is required'),
        age: Yup.number()
            .typeError('Age must be a number')
            .min(18, 'Age must be at least 18 years old')
            .max(99, 'Age must be less than or equal to 99 years old')
            .integer('Must be an integer')
            .required('Age is required'),
        address: Yup.string()
            .matches(/^([a-zA-Z\s]+),\s*([a-zA-Z\s]+),\s*([a-zA-Z\s]+),\s*([\d]+)$/, 'Invalid address format, for exsample valid adress: Ukraine, Kiyv, street Gagarina, 15')
            .required('Adress is required'),
        tel: Yup.string()
            .matches(/^\(\d{3}\) \d{3}-\d{4}$/, 'Invalid phone number')
            .required('Phone number is required'),
        });
    return (
    <Formik
        initialValues={{
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            tel: ''
        }}
        validationSchema={validationSchema}
        onSubmit={ (values) => {
            const info = {
                items: currentCart,
                persenalData: values
            }
            console.log(info)
            dispatch(clearCart(currentCart))
        }}


            
    >
        {({ isSubmitting, errors, touched }) => (
            
            <Form className='form-cart' >
                
                <label htmlFor='firstName'>Your first name</label>
                <Field type='text' name='firstName' placeholder='Enter your first name' className={`${errors.firstName && touched.firstName ? 'input-error' : ''} ${touched.firstName && !errors.firstName ? 'input-valid' : ''}`}/>
                <ErrorMessage name='firstName' component={Error} />

                <label htmlFor='lastName'>Your last name</label>
                <Field type='text' name='lastName' placeholder='Enter your last name' className={`${errors.lastName && touched.lastName ? 'input-error' : ''} ${touched.lastName && !errors.lastName ? 'input-valid' : ''}`}/>
                <ErrorMessage name='lastName' component={Error}  />

                <label htmlFor='age'>Your age</label>
                <Field type='text' name='age' placeholder='Enter your age' className={`${errors.age && touched.age ? 'input-error' : ''} ${touched.age && !errors.age ? 'input-valid' : ''}`} />

                <ErrorMessage name='age' component={Error}  />

                <label htmlFor='address'>Your address for delivery</label>
                <Field type='text' name='address' placeholder='Enter your address' className={`${errors.address && touched.address ? 'input-error' : ''} ${touched.address && !errors.address ? 'input-valid' : ''}`}/>
                <ErrorMessage name='address' component={Error}  />

                <label htmlFor='tel'>Your phone number</label>
                <Field
                    name='tel'
                    render={({ field }) => (
                        <PatternFormat
                            {...field}
                            format='(###) ###-####'
                            placeholder='(___) ___-____'
                            mask='_'
                            className={`${errors.tel && touched.tel ? 'input-error' : ''} ${touched.tel && !errors.tel ? 'input-valid' : ''}`}
                        />
                    )}/>
                <ErrorMessage name='tel' component={Error}  />

                <button className='btn-checkout' type='submit' disabled={isSubmitting}>
                Checkout
                </button>
            </Form>
        )}
    </Formik>
    );

}