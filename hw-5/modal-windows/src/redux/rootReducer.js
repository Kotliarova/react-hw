import { combineReducers } from "redux";
import { itemsReducer as items } from "./reducers/items";
import { modalReducer as modal } from "./reducers/modal";

export const rootReducer = combineReducers({
  items, 
  modal
});
