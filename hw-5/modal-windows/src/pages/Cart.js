import { ListItem } from "../components/ListItem/list-item"
import { useSelector } from "react-redux"
import { FormCart } from "../components/Form/form"
import './forPages.scss'

function Cart(){
    const cart = useSelector(state => state.items.cartArr)
    return (
        cart.length > 0 ? (<div className="cartPage"><ListItem items={cart}/> <FormCart/></div>) : <div className="message">The cart is empty</div>
    )
}
export default Cart