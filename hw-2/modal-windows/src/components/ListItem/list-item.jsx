import { Component } from "react";
import { Item } from "../Item/item";
import './list-item.scss'
import PropTypes from 'prop-types'

export class ListItem extends Component{

    render(){
        const { favorite, cart } = this.props;
        const itemsList = this.props.items.map(item => (
            <Item key={item.article} item={item} favorite={favorite} cart={cart}/>
        ));
        return <div className="list-item">{itemsList}</div>;
    }
}

ListItem.propTypes = {
    favorite: PropTypes.func,
    cart: PropTypes.func,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            article: PropTypes.string
        })
    )
}


ListItem.defaultProps = {
    items: [
      { article: "000", urlPhoto: "#", name: "something", price: 0 }
    ]
  }