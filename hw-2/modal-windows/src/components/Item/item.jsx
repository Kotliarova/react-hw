import { Component } from "react";
import { Button, ButtonAddToCard } from "../Button/button";
import { Modal } from "../Modal/modal";
import { ModalBackground } from "../Modal/modal.styles";
import { svgFavorite, svgFavoriteDone } from '../Header/header'
import PropTypes from 'prop-types'
import './item.scss'

export class Item extends Component{
    constructor(props) {
        super(props)
        this.state = {
            addCard: false,
            isFavorite: false
        }
    }

    addToCard = () => {
        this.setState(prevState => ({
            ...prevState,
            addCard: true
        }));
    }

    openModal = (event) => {
        this.setState(prevState => ({
            ...prevState,
            addCard: true
        }));
    }

    closeModal = (e) => {
        if (e.target === e.currentTarget) { 
            this.setState(prevState => ({
                ...prevState,
                addCard: false
            }));
        }
    }

    addToFav = (obj) => {
        this.setState(prevState => ({
            ...prevState,
            isFavorite: true
        }));
        this.props.favorite(1)
        let favorite = JSON.parse(localStorage.getItem('favorite')) || []; 
        favorite.push(obj);
        localStorage.setItem('favorite', JSON.stringify(favorite))
    }

    deleteFromFav = (obj) => {
        this.setState(prevState => ({
            ...prevState,
            isFavorite: false
        }));
        this.props.favorite(-1)
        let favorite = JSON.parse(localStorage.getItem('favorite')) || []; 
        const currentFav = favorite.filter((elem) => elem.name !== obj.name)
        localStorage.setItem('favorite', JSON.stringify(currentFav))
    }

    render(){
        return (
              <>
              {this.state.addCard && (
              <ModalBackground onClick={this.closeModal}> 
                  <Modal 
                  cart={this.props.cart}
                  item={this.props.item}
                  closeModal={this.closeModal}
                  modalId={'3'}/>
              </ModalBackground>)}
              <div className="item">
                  <img src={this.props.item.urlPhoto} alt={this.props.item.name} />
                  <h2>{this.props.item.name}</h2>
                  <div className="wrap">
                      <div className="item-price">${this.props.item.price}</div>
                      <Button className={'btn-fav-mini'}
                        onClick={() => {this.state.isFavorite ? this.deleteFromFav(this.props.item) : this.addToFav(this.props.item)}}
                        content={this.state.isFavorite ? svgFavoriteDone : svgFavorite}
                        >
                        </Button>
                      <ButtonAddToCard onClick={this.addToCard}></ButtonAddToCard>
                  </div>
              </div>
              </>
        )
    }
}


Item.propTypes = {
    cart: PropTypes.func,
    favorite: PropTypes.func,
    item: PropTypes.shape({
        urlPhoto: PropTypes.string,
        name: PropTypes.string,
        price: PropTypes.string
    })
}


