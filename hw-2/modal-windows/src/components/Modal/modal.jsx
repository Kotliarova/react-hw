import { Component } from 'react';
import { StyleModal, StyleModalHeader, StyleModalText } from './modal.styles';
import { ButtonClose, ButtonAction } from '../Button/button';
import { StyledButtonWrapper } from '../Button/button.styles';
import { collectModals } from './modalData';
import PropTypes from 'prop-types'


export class Modal extends Component {

    addToCard = (obj) => {
        let cart = JSON.parse(localStorage.getItem('cart')) || []; 
        cart.push(obj);
        localStorage.setItem('cart', JSON.stringify(cart));
    }

    render() {
    const { closeButton, closeModal, cart, modalId } = this.props;
    const modal = collectModals.find(item => item.id === modalId);

    return (
      <StyleModal>
        <StyleModalHeader>{modal.header}
        {closeButton ? <ButtonClose 
                onClick={closeModal}
            /> : null}
        </StyleModalHeader>
        <StyleModalText>{modal.text}</StyleModalText>
        <StyledButtonWrapper>
            {modal.actions[0] === 'Yes' ? <ButtonAction action={modal.actions[0]} onClick={(e) => {this.addToCard(this.props.item); cart(1); closeModal(e)}}/> : null}
            {modal.actions.length === 2 ? <ButtonAction action={modal.actions[1]} onClick={closeModal}/> : null}
        </StyledButtonWrapper>
      </StyleModal>
    );
    }
}

Modal.propTypes = {
    closeButton: PropTypes.bool, 
    closeModal: PropTypes.func,
    cart: PropTypes.func,
    modalId: PropTypes.string
}

Modal.defaultProps = {
    closeButton: true
}