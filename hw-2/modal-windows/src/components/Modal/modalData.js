export const collectModals = [
    {
    id: "1",
    header: "Do you want to delete this file?",
    text: "Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete this?",
    actions: ['ok', 'cancel']
    },
    {
    id: "2",
    header: "Do you want to save this file?",
    text: "description for modal 2",
    actions: ['save', 'delete']
    },
    {
    id: "3",
    header: "Do you want to add this product to the cart?",
    text: "Click Yes to add to cart",
    actions: ['Yes', 'No']
    }
    
]
