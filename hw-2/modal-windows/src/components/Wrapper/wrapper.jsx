import { Component } from 'react';
import { StyledWrapper } from './wrapper.styles';
import { collectModals } from '../Modal/modalData';
import { ListItem } from '../ListItem/list-item';
import { Header } from '../Header/header';

class Wrapper extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: {
                openModalId: ''
            },
            items: [],
            countFavorite: 0,
            countCart: 0
        }
    }

    componentDidMount(){
        fetch('./items.json').then((response) => response.json()
        .then(itemsData => {
            this.setState(prevState => ({
                ...prevState,
                items: itemsData
              }));
        }))
    }

    openModal = (event) => {
        const btnId = event.target.id;
        const currentModal = collectModals.find(modal => modal.id === btnId);
        this.setState({ openModalId: currentModal.id });
    }

    closeModal = (e) => {
        if (e.target === e.currentTarget) { 
            this.setState({ openModalId: '' });
        }
    }

    chengeCountFavorite = (num) => {
        this.setState(prevState => ({
            ...prevState,
            countFavorite: prevState.countFavorite + (num)
        }));
        
    }

    chengeCountCart = (num) => {
        this.setState(prevState => ({
            ...prevState,
            countCart: prevState.countCart + (num)
        }));
    }

    componentWillUnmount = () => {
        localStorage.clear()
    }


    render(){
        
        return (
        <StyledWrapper>
            <Header countFavorite={this.state.countFavorite} countCart={this.state.countCart}></Header>
            <ListItem items={this.state.items} favorite={this.chengeCountFavorite} cart={this.chengeCountCart} />
        </StyledWrapper>)
    }
}

export default Wrapper