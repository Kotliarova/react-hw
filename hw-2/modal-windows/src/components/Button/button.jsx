import { Component } from 'react';
import { StyledButtonClose, StyledButtonAction } from './button.styles';
import './button.scss'
import PropTypes from 'prop-types'

export class Button extends Component {
    
    render(){
        const { className, onClick, content, countFavorite, countCart } = this.props;
        return (
        <button className={className}
            onClick={onClick}>
            {content}
            {countFavorite > 0 ? countFavorite : null}
            {countCart > 0 ? countCart : null}             
        </button>)
    }
}

export class ButtonClose extends Button {

    render(){
        const { onClick } = this.props;
        return (
        <StyledButtonClose
            onClick={onClick}>
            {'X'}
        </StyledButtonClose>)
    }
}

export class ButtonAction extends Button {

    render(){
        const { onClick, action } = this.props;
        return (
        <StyledButtonAction
            onClick={onClick}>
            {action}
        </StyledButtonAction>)
    }
}

export class ButtonAddToCard extends Button {

    render(){
        const { onClick } = this.props;
        return (
        <button className="btn-add-to-card"
            onClick={onClick}>
                Add to card
        </button>)
    }
}

Button.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    content: PropTypes.element,
    countFavorite: PropTypes.number,
    countCart: PropTypes.number
}

Button.defaultProps = {
    content: null,
    countFavorite: 0,
    countCart: 0
}
