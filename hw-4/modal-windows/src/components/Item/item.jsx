import { Button } from "../Button/button";
import { Modal } from "../Modal/modal";
import { ModalBackground } from "../Modal/modal.styles";
import { svgFavorite, svgFavoriteDone } from '../Header/header'
import PropTypes from 'prop-types'
import './item.scss'
import { useDispatch, useSelector } from "react-redux";
import { addToFavorite, removeFromFavorite, addCurrentItem, deleteCurrentItem } from "../../redux/actions/items";
import { openModal, closeModalAction } from "../../redux/actions/modal";


export function Item( { item, isFav, inCart }) {
    const modalId = useSelector(state => state.modal.modalId);
    const isModalOpen = useSelector(state => state.modal.isOpenModal);
    const dispatch = useDispatch();

    const addToFav = (item) => {
        dispatch(addToFavorite(item))
    }

    const removeFromFav = (item) => {
        dispatch(removeFromFavorite(item))
    }

    const addToCartWithModal = (item, modalId) => {
        dispatch(addCurrentItem(item))
        dispatch(openModal(modalId))
    }

    const closeModal = (e) => {
        if (e.target === e.currentTarget) {
            dispatch(closeModalAction(modalId))
            dispatch(deleteCurrentItem())
        }
    }

    const removeFromCartWithModal = (item, modalId) => {
        dispatch(addCurrentItem(item))
        dispatch(openModal(modalId))
    }

    return (
        <>
            {isModalOpen && (
            <ModalBackground onClick={(e)=> {closeModal(e)}}> 
                <Modal 
                    item={item}
                    closeModal={(e)=> {closeModal(e)}}
                    />
            </ModalBackground>)}
            <div className="item">
                {inCart ? <Button className={'btn-close btn-close-card'} onClick={() => removeFromCartWithModal(item, '1')} content={'X'}/> : null}
                <img src={item.urlPhoto} alt={item.name} />
                <h2>{item.name}</h2>
                <div className="wrap">
                    <div className="item-price">${item.price}</div>
                    <Button className={'btn-fav-mini'}
                        onClick={() => {isFav ? removeFromFav(item) : addToFav(item)}}
                        content={isFav ? svgFavoriteDone : svgFavorite}>
                    </Button>
                    {!inCart ? <Button className={'btn-add-to-cart'} onClick={() => {addToCartWithModal(item, '3')}} content={'Add to cart'}></Button> : null}
                </div>
            </div>
        </>
    )
}



Item.propTypes = {
    cart: PropTypes.func,
    favorite: PropTypes.func,
    item: PropTypes.shape({
        urlPhoto: PropTypes.string,
        name: PropTypes.string,
        price: PropTypes.string
    })
}


