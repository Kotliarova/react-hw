import { itemsTypes } from "../types";

export function renderItem(items) {
  return {
    type: itemsTypes.RENDER_ITEM,
    payload: items
  };
}

export function fetchAllItemsFromServer() {
  return (dispatch) => {
    try{
      fetch('./items.json')
      .then(res => res.json())
      .then(items => {
        dispatch(renderItem(items))})
    }
    catch(err) {
      console.log(err)
    }
  }
}

export function fetchFavoriteFromLocalStorage() {
  return (dispatch) => {
    try{
      const currentFav = JSON.parse(localStorage.getItem('favorite')) || []
      dispatch(getFavoriteFromLocalStorage(currentFav))
    }
    catch(err) {
      console.log(err)
    }
  }
}

function getFavoriteFromLocalStorage(favorites) {
  return {
    type: itemsTypes.GET_FAVORITES,
    payload: favorites
  }
}

export function fetchCartFromLocalStorage() {
  return (dispatch) => {
    try {
      const currentCart = JSON.parse(localStorage.getItem('cart')) || []
      dispatch(getCartFromLocalStorage(currentCart))
    }
    catch(err) {
      console.log(err)
    }
  }
}
function getCartFromLocalStorage(cart) {
  return {
    type: itemsTypes.GET_CART,
    payload: cart
  }
}

export function addToFavorite(item) {
  const currentFavLocalStoredge = JSON.parse(localStorage.getItem('favorite')) || []
  currentFavLocalStoredge.push(item)
  localStorage.setItem('favorite', JSON.stringify(currentFavLocalStoredge))
  return {
    type: itemsTypes.ADD_TO_FAVORITE,
    payload: { item }
  }
}

export function removeFromFavorite(item) {
  const currentFavLocalStoredge = JSON.parse(localStorage.getItem('favorite'))
  const currentItemForRemove =  currentFavLocalStoredge.findIndex((obj) => obj.article === item.article)
  const updateFav = [...currentFavLocalStoredge.slice(0, currentItemForRemove), ...currentFavLocalStoredge.slice(currentItemForRemove + 1)]
  localStorage.setItem('favorite', JSON.stringify(updateFav))
  return {
    type: itemsTypes.REMOVE_FROM_FAVORITE,
    payload: { item }
  }
}

export function addToCart(item) {
  const currentCartLocalStoredge = JSON.parse(localStorage.getItem('cart')) || []
  currentCartLocalStoredge.push(item)
  localStorage.setItem('cart', JSON.stringify(currentCartLocalStoredge))
  return {
    type: itemsTypes.ADD_TO_CART,
    payload: { item }
  }
}

export function addCurrentItem(item) {
  return {
    type: itemsTypes.ADD_CURRENT_ITEM,
    payload: { item }
  }
}

export function deleteCurrentItem() {
  return {
    type: itemsTypes.DELETE_CURRENT_ITEM,
  }
}

export function removeFromCart(item) {
  const currentCartLocalStoredge = JSON.parse(localStorage.getItem('cart'))
  const currentItemForRemove =  currentCartLocalStoredge.findIndex((obj) => obj.article === item.article)
  const updateCart = [...currentCartLocalStoredge.slice(0, currentItemForRemove), ...currentCartLocalStoredge.slice(currentItemForRemove + 1)]
  localStorage.setItem('cart', JSON.stringify(updateCart))
  return {
    type: itemsTypes.REMOVE_FROM_CART,
    payload: { item }
  }
}
