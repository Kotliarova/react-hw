import { modalTypes } from "../types";

export function openModal( modalId) {
    return {
      type: modalTypes.OPEN_MODAL,
      payload: {
        modalId
    }
    };
  }
  
  export function closeModalAction( modalId) {
    return {
      type: modalTypes.CLOSE_MODAL,
      payload: {
        modalId
    }
    };
  }
  