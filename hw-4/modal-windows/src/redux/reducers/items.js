import { itemsTypes } from "../types";
const initialState = {
  itemsArr: [],
  favoriteArr: [],
  cartArr: [],
  currentItem: null,
  loadedFromServer: false
}

export function itemsReducer(state = initialState, action) {
  switch (action.type) {
    case itemsTypes.RENDER_ITEM:
      return {
        ...state,
        itemsArr: action.payload,
        loadedFromServer: true
      };
    case itemsTypes.ADD_TO_FAVORITE:
      const newFavArr = [...state.favoriteArr, action.payload.item]
      return {
        ...state,
        favoriteArr: newFavArr
      }
    case itemsTypes.REMOVE_FROM_FAVORITE:
      const currentFav = state.favoriteArr.findIndex((obj) => obj.article === action.payload.item.article)
      const updateFav = [...state.favoriteArr.slice(0, currentFav), ...state.favoriteArr.slice(currentFav + 1)]
      return {
        ...state,
        favoriteArr: updateFav

      }
    case itemsTypes.ADD_TO_CART:
      const newCartArr = [...state.cartArr, action.payload.item]
      return {
        ...state,
        cartArr: newCartArr
      }
      case itemsTypes.ADD_CURRENT_ITEM:
        return {
          ...state,
          currentItem: action.payload.item
        }
      case itemsTypes.DELETE_CURRENT_ITEM:
        return {
          ...state,
          currentItem: null
        }
      case itemsTypes.REMOVE_FROM_CART:
        const currentInCart = state.cartArr.findIndex((obj) => obj.article === action.payload.item.article)
        const updateCart = [...state.cartArr.slice(0, currentInCart),...state.cartArr.slice(currentInCart + 1)]
        return {
          ...state,
          cartArr: updateCart
        }
      case itemsTypes.GET_FAVORITES: 
        return {
          ...state,
          favoriteArr: action.payload
        }
      case itemsTypes.GET_CART: 
      return {
        ...state,
        cartArr: action.payload
      }
    default:
      return state;
  }
}