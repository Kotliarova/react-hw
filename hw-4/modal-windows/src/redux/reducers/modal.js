import { modalTypes } from "../types";

const initialState = {
    isOpenModal: false,
    modalId: ''
}
export function modalReducer(state = initialState, action) {
    switch (action.type) {
        case modalTypes.OPEN_MODAL:
            return {
                isOpenModal: true,
                modalId: action.payload.modalId,
            }
        case modalTypes.CLOSE_MODAL:
            return {
                isOpenModal: false,
                modalId: ''
            }
        default:
            return state;
    }
}