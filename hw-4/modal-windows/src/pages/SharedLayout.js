import { Outlet } from 'react-router-dom';
import { Header } from '../components/Header/header';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from "react";
import { fetchAllItemsFromServer, fetchFavoriteFromLocalStorage , fetchCartFromLocalStorage} from '../redux/actions/items';

const SharedLayout = () => {
  const loadedFromServer = useSelector(state => state.items.loadedFromServer);
  const dispatch = useDispatch();
  useEffect(() => {
    if (!loadedFromServer) {
      dispatch(fetchAllItemsFromServer())
      dispatch(fetchFavoriteFromLocalStorage())
      dispatch(fetchCartFromLocalStorage())
    }
  }, [dispatch, loadedFromServer]);

  return (
    <>
      <Header />
      <section>
        <Outlet />
      </section>
    </>
  );
};

export default SharedLayout;