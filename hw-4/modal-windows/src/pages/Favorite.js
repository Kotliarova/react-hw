import { ListItem } from "../components/ListItem/list-item"
import { useSelector } from "react-redux"
import './message.scss'


function Favorite(){
    const favorites = useSelector(state => state.items.favoriteArr)
    return (
       favorites.length > 0 ? <ListItem items={favorites}/> : <div className="message">Nothing has been added to your favorites yet</div>
    )
}

export default Favorite