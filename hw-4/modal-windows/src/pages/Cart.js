import { ListItem } from "../components/ListItem/list-item"
import { useSelector } from "react-redux"

function Cart(){
    const cart = useSelector(state => state.items.cartArr)
    return (
        cart.length > 0 ? <ListItem items={cart}/> : <div className="message">The cart is empty</div>
    )
}
export default Cart