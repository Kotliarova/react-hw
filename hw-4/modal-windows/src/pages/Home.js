import Wrapper from '../components/Wrapper/wrapper'
import { useSelector } from "react-redux";

function Home(){
    const items = useSelector(state => state.items.itemsArr);
    return (
        <Wrapper items={items}/>
    )
}

export default Home