import './App.css';
import { Route, Routes } from 'react-router-dom';
import SharedLayout from './pages/SharedLayout';
import Home from './pages/Home';
import Favorite from './pages/Favorite';
import Cart from './pages/Cart'

function App() {
  return (
      <Routes>
        <Route path='/' element={<SharedLayout/>}>
          <Route index element={<Home/>}/>
          <Route path='favorite' element={<Favorite/>}/>
          <Route path='cart' element={<Cart/>}/>
        </Route>
      </Routes>
  );
}

export default App;


